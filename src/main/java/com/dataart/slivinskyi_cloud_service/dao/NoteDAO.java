package com.dataart.slivinskyi_cloud_service.dao;

import com.dataart.slivinskyi_cloud_service.model.Note;

import java.sql.*;

public class NoteDAO {

    public int addNote(Note note) throws SQLException, ClassNotFoundException {
        String InsertNoteSQL = "INSERT INTO NOTES" + " (notetitle, notedescription, userid) VALUES " + " (?,?,?);";
        int result = 0;

        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(InsertNoteSQL);
        preparedStatement.setString(1, note.getNoteTitle());
        preparedStatement.setString(2, note.getNoteDescription());
        preparedStatement.setInt(3, note.getUserId());
        System.out.println(preparedStatement);
        result = preparedStatement.executeUpdate();

        return result;
    }

    public Note getNoteById(int noteid) throws SQLException, ClassNotFoundException {
        String SelectNoteById = "SELECT * FROM NOTES WHERE noteid=?;";
        Note note = new Note();

            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SelectNoteById);
            preparedStatement.setInt(1, noteid);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                note.setNoteId(resultSet.getInt("noteid"));
                note.setNoteTitle(resultSet.getString("notetitle"));
                note.setNoteDescription(resultSet.getString("notedescription"));
                note.setUserId(resultSet.getInt("userid"));
            }

        return note;
    }

    public int updateNote(Note note) throws SQLException, ClassNotFoundException {
        String UpdateNoteSQL = "UPDATE NOTES SET notetitle=?, notedescription=? WHERE noteid=?;";
        int result = 0;

        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(UpdateNoteSQL);
        preparedStatement.setString(1, note.getNoteTitle());
        preparedStatement.setString(2, note.getNoteDescription());
        preparedStatement.setInt(3, note.getNoteId());
        result = preparedStatement.executeUpdate();

        return result;
    }


    public boolean deleteNote (int noteid) throws SQLException, ClassNotFoundException {

        boolean rowDeleted = false;
        String deleteNoteSQL = "DELETE FROM NOTES WHERE noteid = " + "(?);";

        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(deleteNoteSQL);
        preparedStatement.setInt(1, noteid);
        if (preparedStatement.executeUpdate() > 0) ;
        rowDeleted = true;

        return rowDeleted;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cloud_schema", "root", "slivinska1999");
        return connection;
    }

    }


