package com.dataart.slivinskyi_cloud_service.dao;

import com.dataart.slivinskyi_cloud_service.model.Credential;
import java.sql.*;

public class CredentialDAO {

    public int insertCredential(Credential credential) throws SQLException, ClassNotFoundException {
        String InsertCredentialSQL = "INSERT INTO CREDENTIALS (url, username, password, kye, userid) VALUES (?,?,?,?,?);";
        int result = 0;

            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(InsertCredentialSQL);
            preparedStatement.setString(1, credential.getUrl());
            preparedStatement.setString(2, credential.getUsername());
            preparedStatement.setString(3, credential.getPassword());
            preparedStatement.setString(4, credential.getKey());
            preparedStatement.setInt(5, credential.getUserId());

            System.out.println(preparedStatement);
            result = preparedStatement.executeUpdate();

        return result;
    }

    public int updateCredential(Credential credential) throws SQLException, ClassNotFoundException {
        String UpdateCredentialSQL = "UPDATE CREDENTIALS SET url=?, username=?, password=? WHERE credentialid=?;";
        int result = 0;

            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UpdateCredentialSQL);
            preparedStatement.setString(1, credential.getUrl());
            preparedStatement.setString(2, credential.getUsername());
            preparedStatement.setString(3, credential.getPassword());
            preparedStatement.setInt(4, credential.getCredentialId());

            System.out.println(preparedStatement);
            result = preparedStatement.executeUpdate();

        return result;
    }

    public int deleteCredential(int credentialId) throws SQLException, ClassNotFoundException {
        String DeleteCredentialSQL = "DELETE FROM CREDENTIALS WHERE credentialid=?;";
        int result = 0;

            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DeleteCredentialSQL);
            preparedStatement.setInt(1, credentialId);
            System.out.println(preparedStatement);
            result = preparedStatement.executeUpdate();

        return result;
    }

    public Credential getCredentialsById(int credentialId) throws SQLException, ClassNotFoundException {
        String SelectCredentialSQL = "SELECT * FROM CREDENTIALS WHERE credentialid=?;";
        Credential credential = new Credential();

            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SelectCredentialSQL);
            preparedStatement.setInt(1, credentialId);
            System.out.println(preparedStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                credential.setCredentialId(resultSet.getInt("credentialid"));
                credential.setUrl(resultSet.getString("url"));
                credential.setUsername(resultSet.getString("username"));
                credential.setPassword(resultSet.getString("password"));
                credential.setKey(resultSet.getString("kye"));
                credential.setUserId(resultSet.getInt("userid"));
            }
        return credential;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cloud_schema", "root", "slivinska1999");
        return connection;
    }

}
