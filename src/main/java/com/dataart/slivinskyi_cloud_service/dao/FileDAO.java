package com.dataart.slivinskyi_cloud_service.dao;

import com.dataart.slivinskyi_cloud_service.model.File;

import java.sql.*;

public class FileDAO {

    public int insertFile(File file) throws SQLException, ClassNotFoundException {
        String InsertFileSQL = "INSERT INTO FILES (filename, filedata, description, userid) VALUES " + "(?,?,?,?);";
        int result = 0;

        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(InsertFileSQL);
        preparedStatement.setString(1, file.getFileName());
        preparedStatement.setBytes(2, file.getFileData());
        preparedStatement.setString(3, file.getDescription());
        preparedStatement.setInt(4, file.getUserId());

        System.out.println(preparedStatement);
        result = preparedStatement.executeUpdate();

        return result;
    }

    public File selectFile(int fileid) throws SQLException, ClassNotFoundException {
        String SelectFileSQL = "SELECT * FROM FILES WHERE fileid=?;";
        File file = new File();

            Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SelectFileSQL);
            preparedStatement.setInt(1, fileid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                file.setFileId(resultSet.getInt("fileid"));
                file.setFileName(resultSet.getString("filename"));
                file.setDescription(resultSet.getString("description"));
                file.setUserId(resultSet.getInt("userid"));
                file.setFileData( resultSet.getBytes("filedata"));
            }
        return file;
    }

    public int deleteFile(int fileid) throws SQLException, ClassNotFoundException {
        String DeleteFileSQL = "DELETE FROM FILES WHERE fileid=(?);";
        int result = 0;
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(DeleteFileSQL);
        preparedStatement.setInt(1,fileid);
        result = preparedStatement.executeUpdate();
        return result;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cloud_schema", "root", "slivinska1999");
        return connection;
    }
}
