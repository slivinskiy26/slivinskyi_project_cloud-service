package com.dataart.slivinskyi_cloud_service.dao;

import com.dataart.slivinskyi_cloud_service.model.User;
import com.dataart.slivinskyi_cloud_service.service.HashService;

import java.security.SecureRandom;
import java.sql.*;
import java.util.Base64;

public class UserDAO {

    private final HashService hashService = new HashService();

    public int registerUser(User user) throws ClassNotFoundException, SQLException {

        String InsertUserSQL = "INSERT INTO USER" + " (username, password, firstname, lastname, salt) VALUES " + " (?,?,?,?,?);";
        int result = 0;

        Connection connection = getConnection();

        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        String encodedSalt = Base64.getEncoder().encodeToString(salt);
        String hashedPassword = hashService.getHashedValue(user.getPassword(), encodedSalt);

        PreparedStatement preparedStatement = connection.prepareStatement(InsertUserSQL);
        preparedStatement.setString(1, user.getUsername());
        preparedStatement.setString(2, hashedPassword);
        preparedStatement.setString(3, user.getFirstName());
        preparedStatement.setString(4, user.getLastName());
        preparedStatement.setString(5, encodedSalt);

        System.out.println(preparedStatement);

        result = preparedStatement.executeUpdate();

        return result;
    }

    public User findUserByUsername(String username) throws ClassNotFoundException, SQLException {
        String FindUserSQL = "SELECT * FROM USER WHERE USERNAME = " + "(?);";
        User user = new User();

        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(FindUserSQL);
        preparedStatement.setString(1, username);

        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            user.setUserId(resultSet.getInt("userid"));
            user.setFirstName(resultSet.getString("firstname"));
            user.setLastName(resultSet.getString("lastname"));
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setSalt(resultSet.getString("salt"));
        }

        return user;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/cloud_schema", "root", "slivinska1999");
        return connection;
    }


}
