package com.dataart.slivinskyi_cloud_service.model;

public class File {
    private Integer fileId;
    private String fileName;
    private String description;
    private byte[] fileData;
    private Integer userId;

    public File() {
    }

    public File(Integer fileId, String fileName, String description, byte[] fileData, Integer userId) {
        this.fileId = fileId;
        this.fileName = fileName;
        this.description = description;
        this.fileData = fileData;
        this.userId = userId;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
