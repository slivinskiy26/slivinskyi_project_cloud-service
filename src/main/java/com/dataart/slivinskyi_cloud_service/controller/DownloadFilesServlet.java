package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.FileDAO;
import com.dataart.slivinskyi_cloud_service.model.File;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DownloadFilesServlet", value = "/fileDownload")
public class DownloadFilesServlet extends HttpServlet {
    private final FileDAO fileDAO = new FileDAO();
    private static final Logger LOGGER = Logger.getLogger(UploadFilesServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int fileid = Integer.parseInt(request.getParameter("fileid"));
        try {
            File file = fileDAO.selectFile(fileid);
            if (file == null) {
                LOGGER.error("There was a problem downloading the file");
                request.setAttribute("errorDownloadFile", "The file can`t be viewed");
                RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
                dispatcher.forward(request, response);
            } else {
                LOGGER.info("The file was downloaded successfully");
                String contentType = this.getServletContext().getMimeType(file.getFileName());
                response.setHeader("Content-Type", contentType);
                response.setHeader("Content-Length", file.getFileData().toString());
                response.setHeader("Content-Disposition", "inline; filename=\"" + file.getFileName() + "\"");
                response.getOutputStream().write(file.getFileData());
            }
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Error with your request. Exception: {}", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
