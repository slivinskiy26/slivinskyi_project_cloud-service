package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.NoteDAO;
import com.dataart.slivinskyi_cloud_service.dao.UserDAO;
import com.dataart.slivinskyi_cloud_service.model.Note;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "EditNotesServlet", value = "/updateNote")
public class EditNotesServlet extends HttpServlet {

    private final NoteDAO noteDAO = new NoteDAO();
    private static final int MIN_SQL_RESULT = 0;
    private static final Logger LOGGER = LogManager.getLogger(NewNotesServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("noteid"));
        try {
            Note existingNote = noteDAO.getNoteById(id);
            RequestDispatcher dispatcher = request.getRequestDispatcher("note-form.jsp");
            request.setAttribute("note", existingNote);
            dispatcher.forward(request, response);
        } catch (SQLException | ClassNotFoundException e){
            LOGGER.error("Error while updating the note. Exception: {}", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Note note = noteDAO.getNoteById(Integer.parseInt(request.getParameter("noteid")));
            note.setNoteTitle(request.getParameter("noteTitle"));
            note.setNoteDescription(request.getParameter("noteDescription"));

            int result = noteDAO.updateNote(note);
            if (result > MIN_SQL_RESULT) {
                LOGGER.info("The note has been updated");
                request.setAttribute("successUpdateNote", "The note has been updated.");
                RequestDispatcher dispatcher = request.getRequestDispatcher("notes.jsp");
                dispatcher.forward(request, response);
            } else {
                LOGGER.error("There was a problem updating the note");
                request.setAttribute("errorUpdateNote", "There is a problem while updating the note. Try again later");
                RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException | ClassNotFoundException e){
            LOGGER.error("Error while updating the note. Exception: {}", e);
        }
    }
}
