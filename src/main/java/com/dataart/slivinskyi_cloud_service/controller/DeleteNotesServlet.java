package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.NoteDAO;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DeleteNotesServlet", value = "/deleteNote")
public class DeleteNotesServlet extends HttpServlet {

    private final NoteDAO noteDAO = new NoteDAO();
    private static final Logger LOGGER = LogManager.getLogger(NewNotesServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("noteid"));
            Boolean success = noteDAO.deleteNote(id);
            if (success == true) {
                LOGGER.info("The note was deleted");
                request.setAttribute("successDeleteNote", "The note has been deleted");
                RequestDispatcher dispatcher = request.getRequestDispatcher("notes.jsp");
                dispatcher.forward(request, response);
            } else {
                LOGGER.error("There was a problem deleting the note");
                request.setAttribute("errorDeleteNote", "There is a problem while deleting the note. Try again later");
                RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException | ClassNotFoundException e){
            LOGGER.error("Error while deleting the note. Exception: {}", e);
        }
    }

}
