package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.UserDAO;
import com.dataart.slivinskyi_cloud_service.model.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "SignUpServlet", value = "/signup")
public class SignUpServlet extends HttpServlet {

    private final UserDAO userDAO = new UserDAO();
    private static final int MIN_SQL_RESULT = 0;
    private static final Logger LOGGER = LogManager.getLogger(SignUpServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("signup.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            if (userDAO.findUserByUsername(request.getParameter("username")).getUsername() == null) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                String firstName = request.getParameter("firstName");
                String lastName = request.getParameter("lastName");

                User user = new User();
                user.setUsername(username);
                user.setPassword(password);
                user.setFirstName(firstName);
                user.setLastName(lastName);

                if (userDAO.registerUser(user) > MIN_SQL_RESULT) {
                    LOGGER.info("Congratulations " + username + " Your account have been successfully created!");
                    request.setAttribute("successSignUp", "Congratulations, " + username + "! Your account have been successfully created!");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
                    dispatcher.forward(request, response);
                } else {
                    LOGGER.error("Sorry " + firstName + ", there was a problem to register your account, please try again.");
                    request.setAttribute("errorSignup", "There is problem while creating you an account");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("signup.jsp");
                    dispatcher.forward(request, response);
                }
            } else {
                LOGGER.info("This username already exists, try another one.");
                request.setAttribute("invalidUsername", "This username already exists, try another one." );
                RequestDispatcher dispatcher = request.getRequestDispatcher("signup.jsp");
                dispatcher.forward(request, response);
            }
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.error("Error while saving user! Exception: {}", e);
            e.printStackTrace();
        }

    }

}

