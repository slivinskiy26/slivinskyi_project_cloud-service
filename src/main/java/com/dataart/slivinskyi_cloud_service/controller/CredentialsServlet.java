package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.CredentialDAO;
import com.dataart.slivinskyi_cloud_service.model.Credential;
import com.dataart.slivinskyi_cloud_service.service.EncryptionService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.Base64;

@WebServlet(name = "CredentialsServlet", value = "/")
public class CredentialsServlet extends HttpServlet {

    private final EncryptionService encryptionService = new EncryptionService();
    private final CredentialDAO credentialDAO = new CredentialDAO();
    private static final Logger LOGGER = LogManager.getLogger(CredentialsServlet.class);
    private static final int MIN_SQL_RESULT = 0;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String action = request.getServletPath();
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/addCredentials":
                    addCredentials(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/delete":
                    deleteCredentials(request, response);
                    break;
                case "/updateCredentials":
                    updateCredentials(request, response);
                    break;
                default:
                    RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.error("Error with your request. Exception: {}", e);
        }
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("credentials-form.jsp");
        dispatcher.forward(request, response);
    }

    private void addCredentials(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException {
        Credential credential = new Credential();
        credential.setUrl(request.getParameter("credentialsURL"));
        credential.setUsername(request.getParameter("credentialsUsername"));

        HttpSession session = request.getSession();
        credential.setUserId(Integer.parseInt(session.getAttribute("userid").toString()));

        SecureRandom random = new SecureRandom();
        byte[] key = new byte[16];
        random.nextBytes(key);
        String encodedKey = Base64.getEncoder().encodeToString(key);
        String encryptedPassword = encryptionService.encryptValue(request.getParameter("credentialsPassword"), encodedKey);
        String decryptedPassword = encryptionService.decryptValue(encryptedPassword, encodedKey);
        session.setAttribute("decryptedPassword", decryptedPassword);

        credential.setKey(encodedKey);
        credential.setPassword(encryptedPassword);

        int result = credentialDAO.insertCredential(credential);
        if (result > MIN_SQL_RESULT){
            LOGGER.info("The credentials have been successfully saved");
            request.setAttribute("successAddCredentials", "Credentials have been saved");
            RequestDispatcher dispatcher = request.getRequestDispatcher("credentials.jsp");
            dispatcher.forward(request, response);
        } else {
            LOGGER.error("There was a problem saving the credentials");
            request.setAttribute("errorAddCredentials", "There is a problem creating credentials");
            RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
        }
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException {
        int id = Integer.parseInt(request.getParameter("credentialId"));
        Credential existingCredential = credentialDAO.getCredentialsById(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("credentials-form.jsp");
        String decryptedPassword = encryptionService.decryptValue(existingCredential.getPassword(), existingCredential.getKey());
        request.setAttribute("decryptedPassword", decryptedPassword);
        request.setAttribute("credentials", existingCredential);
        dispatcher.forward(request, response);
    }

    private void updateCredentials(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException {
        Credential credential = credentialDAO.getCredentialsById(Integer.parseInt(request.getParameter("credentialId")));
        credential.setUrl(request.getParameter("credentialsURL"));
        credential.setUsername(request.getParameter("credentialsUsername"));
        String encryptedPassword = encryptionService.encryptValue(request.getParameter("credentialsPassword"), credential.getKey());
        credential.setPassword(encryptedPassword);

        int result = credentialDAO.updateCredential(credential);

        if (result > MIN_SQL_RESULT){
            LOGGER.info("The credentials have been updated successfully");
            request.setAttribute("successUpdateCredentials", "Credentials have been updated");
            RequestDispatcher dispatcher = request.getRequestDispatcher("credentials.jsp");
            dispatcher.forward(request, response);
        } else {
            LOGGER.error("There is a problem while updating the credentials");
            request.setAttribute("errorUpdateCredentials", "There is a problem updating credentials. Please try again later!");
            RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
        }
    }

    private void deleteCredentials(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException {
        int credentialId = Integer.parseInt(request.getParameter("credentialId"));
        int result = credentialDAO.deleteCredential(credentialId);
        if (result > MIN_SQL_RESULT){
            LOGGER.info("The credentials have been deleted");
            request.setAttribute("successDeleteCredentials", "Credentials have been deleted");
            RequestDispatcher dispatcher = request.getRequestDispatcher("credentials.jsp");
            dispatcher.forward(request, response);
        } else {
            LOGGER.error("There was a problem deleting the credentials");
            request.setAttribute("errorDeleteCredentials", "There is a problem deleting credentials. Please try again later!");
            RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
        }
    }

}
