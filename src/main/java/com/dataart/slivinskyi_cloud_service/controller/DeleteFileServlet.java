package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.FileDAO;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DeleteFileServlet", value = "/fileDelete")
public class DeleteFileServlet extends HttpServlet {
    private final FileDAO fileDAO = new FileDAO();
    private static final int MIN_SQL_RESULT = 0;
    private static final Logger LOGGER = Logger.getLogger(UploadFilesServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("files.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int fileid = (Integer.parseInt(request.getParameter("fileid")));
        try {
            int result = fileDAO.deleteFile(fileid);
            if (result > MIN_SQL_RESULT) {
                LOGGER.info("The file was deleted successfully");
                request.setAttribute("successDeleteFile", "The file has been deleted");
                RequestDispatcher dispatcher = request.getRequestDispatcher("files.jsp");
                dispatcher.forward(request, response);
            } else {
                LOGGER.error("There was a problem deleting tha file");
                request.setAttribute("errorDeleteFile", "There is a problem while deleting the file");
                RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException | ClassNotFoundException e){
            LOGGER.error("Error with your request. Exception: {}", e);
        }

    }
}
