package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.UserDAO;
import com.dataart.slivinskyi_cloud_service.model.User;
import com.dataart.slivinskyi_cloud_service.service.HashService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginServlet extends HttpServlet {

    private final HashService hashService = new HashService();
    private UserDAO userDAO = new UserDAO();
    private static final Logger LOGGER = LogManager.getLogger(LoginServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("username", request.getParameter("username"));
        try {
            User user = userDAO.findUserByUsername(request.getParameter("username"));
            if (user.getSalt() != null) {
                session.setAttribute("userid", user.getUserId());
                String hashedPassword = hashService.getHashedValue(request.getParameter("password"), user.getSalt());

                if (user != null && user.getPassword().equals(hashedPassword)) {
                    session.setAttribute("loginTrue", "right credentials");
                    LOGGER.info("You successfully logged in! ");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
                    dispatcher.forward(request, response);
                } else {
                    LOGGER.error("There was a problem to login you.\n Username or Password incorrect");
                    session.setAttribute("errorMessage", "username or password is wrong. Please, try again!");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
                    dispatcher.forward(request, response);
                }
            } else {
                LOGGER.error("Incorrect username");
                session.setAttribute("IncorrectUsername", "Username is wrong!");
                RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
                dispatcher.forward(request, response);
            }
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.error("Error when trying to login. Exception: {}", e);
            e.printStackTrace();
        }
    }
}
