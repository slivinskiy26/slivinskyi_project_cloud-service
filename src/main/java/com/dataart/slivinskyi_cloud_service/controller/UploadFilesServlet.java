package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.FileDAO;
import com.dataart.slivinskyi_cloud_service.model.File;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UploadFilesServlet", value = "/fileUpload")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50) // 50MB
public class UploadFilesServlet extends HttpServlet {

    private final FileDAO fileDAO = new FileDAO();
    private static final int MIN_SQL_RESULT = 0;
    private static final Logger LOGGER = Logger.getLogger(UploadFilesServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("files.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userid");
        String description = request.getParameter("description");
        Part part = request.getPart("file");
        String fileName = extractFileName(part);

        com.dataart.slivinskyi_cloud_service.model.File file = new com.dataart.slivinskyi_cloud_service.model.File();
        file.setFileName(fileName);
        file.setDescription(description);
        file.setUserId(userId);
        file.setFileData(part.getInputStream().readAllBytes());

        try {
            int result = 0;
                result = fileDAO.insertFile(file);
            if (result > MIN_SQL_RESULT) {
                LOGGER.info("The file has been successfully uploaded");
                request.setAttribute("successUploadFile", "The file has been uploaded");
                RequestDispatcher dispatcher = request.getRequestDispatcher("files.jsp");
                dispatcher.forward(request, response);
            } else {
                LOGGER.error("There is a problem while uploading the file");
                request.setAttribute("errorUploadFile", "There is a problem while uploading the file");
                RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
                dispatcher.forward(request, response);
            }
        } catch (ClassNotFoundException  | SQLException e){
            LOGGER.error("Error with your request. Exception: {}", e);
        }

    }


    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                String clientFileName = s.substring(s.indexOf("=") + 2, s.length() - 1);
                clientFileName = clientFileName.replace("\\", "/");
                int i = clientFileName.lastIndexOf('/');
                return clientFileName.substring(i + 1);
            }
        }
        return null;
    }
}
