package com.dataart.slivinskyi_cloud_service.controller;

import com.dataart.slivinskyi_cloud_service.dao.NoteDAO;
import com.dataart.slivinskyi_cloud_service.dao.UserDAO;
import com.dataart.slivinskyi_cloud_service.model.Note;
import com.dataart.slivinskyi_cloud_service.model.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "NewNotesServlet", value = "/newNote")
public class NewNotesServlet extends HttpServlet {

    private final NoteDAO noteDAO = new NoteDAO();
    private final UserDAO userDAO = new UserDAO();
    private static final int MIN_SQL_RESULT = 0;
    private static final Logger LOGGER = LogManager.getLogger(NewNotesServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("note-form.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Note note = new Note();
        note.setNoteTitle(request.getParameter("noteTitle"));
        note.setNoteDescription(request.getParameter("noteDescription"));
        try {
            HttpSession session = request.getSession();
            String username = session.getAttribute("username").toString();
            User user = userDAO.findUserByUsername(username);
            note.setUserId(user.getUserId());

            if (noteDAO.addNote(note) > MIN_SQL_RESULT) {
                LOGGER.info("The note has been successfully created!");
                request.setAttribute("successAddNote", "The note has been creating.");
                RequestDispatcher dispatcher = request.getRequestDispatcher("notes.jsp");
                dispatcher.forward(request, response);
            } else {
                LOGGER.error("There was a problem to create the note.");
                request.setAttribute("errorAddNote", "There is a problem while adding the note. Try again later!");
                RequestDispatcher dispatcher = request.getRequestDispatcher("errorPage.jsp");
                dispatcher.forward(request, response);
            }
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.error("Error while adding the note. Exception: {}", e);
        }
    }
}