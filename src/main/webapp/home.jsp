<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 05.02.2021
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Title</title>
</head>
<body>
<div class="container">
    <h1 style="text-align:center">Welcome to home page!</h1>
    <div  id="logoutDiv">
        <form action="logout">
            <button id="logout" type="submit" class="btn btn-danger pull-right">Logout</button><br>
        </form>
    </div>
    <br>
    <nav style="text-align:center; font-size: 20px">
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a id="files-link" href="files.jsp">Files</a> |
            <a id="notes-link" href="notes.jsp">Notes</a> |
            <a id="credentials-link" href="credentials.jsp">Credentials</a>
        </div>
    </nav>

    <%
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        if (session.getAttribute("loginTrue") == null)
            response.sendRedirect("login.jsp");
    %>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
