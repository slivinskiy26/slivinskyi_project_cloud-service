<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 05.02.2021
  Time: 13:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Title</title>
    <script type="text/javascript">
        function validate(){
            var firstName = document.getElementById("firstName");
            var lastName = document.getElementById("lastName");
            var username = document.getElementById("username");
            var password = document.getElementById("password");

            if (firstName.value.trim()==""){
                alert("Blank First Name")
                firstName.style.border = "solid 3px red";
                document.getElementById("lblFirstName").style.visibility="visible";
                return false;
            }else if (lastName.value.trim()==""){
                alert("Blank Last Name")
                lastName.style.border = "solid 3px red";
                document.getElementById("lblLastName").style.visibility="visible";
                return false;
            }else if (username.value.trim()==""){
                alert("Blank Last Username")
                username.style.border = "solid 3px red";
                document.getElementById("lblUsername").style.visibility="visible";
                return false;
            }else if (password.value.trim()==""){
                alert("Blank Password")
                password.style.border = "solid 3px red";
                document.getElementById("lblPassword").style.visibility="visible";
                return false;
            }else if (password.value.trim().length < 7){
                alert("Password too short. \nPassword should contain at least 7 characters.")
                password.style.border = "solid 3px red";
                document.getElementById("lblPassword").style.visibility="visible";
                return false;
            } else {
                return true;
            }
        }
    </script>
</head>
<body>
<div style="text-align: center">
    <c:if test="${errorSignup != null}">
        <div id="error-msg" class="alert alert-dark">
                ${errorSignup}
        </div>
    </c:if>
    <c:if test="${invalidUsername != null}">
        <div id="error-msg" class="alert alert-dark">
                ${invalidUsername}
        </div>
    </c:if>

    <h1>Sign Up</h1>
    <form onsubmit="return validate();" action="signup" method="post">
        First Name: <input type="text" name="firstName" id="firstName"> <label id="lblFirstName"
                                                                               style="color: red; visibility: hidden;">Invalid</label> <br>
        <br>
        Last Name:  <input type="text" name="lastName" id="lastName"> <label id="lblLastName"
                                                                            style="color: red; visibility: hidden;">Invalid</label> <br>
        <br>
        Username:   <input type="text" name="username" id="username"> <label id="lblUsername"
                                                                           style="color: #ff0000; visibility: hidden;">Invalid</label> <br>
        <br>
        Password:   <input type="password" name="password" id="password"> <label id="lblPassword"
                                                                               style="color: red; visibility: hidden;">Invalid</label> <br>
        <br>
        <button type="submit" name="signup" class="btn btn-success">SignUp</button>
    </form>
    <label> <a id="login-link" href="login.jsp">Back to login</a> </label>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
