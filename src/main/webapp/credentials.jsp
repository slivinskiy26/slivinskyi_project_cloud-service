<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 09.02.2021
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Credentials</title>
</head>
<body>
<div>
    <c:if test="${successAddCredentials != null}">
        <h4>${successAddCredentials}</h4>
    </c:if>
    <c:if test="${successUpdateCredentials != null}">
        <h4>${successUpdateCredentials}</h4>
    </c:if>
    <c:if test="${successDeleteCredentials != null}">
        <h4>${successDeleteCredentials}</h4>
    </c:if>

    <%  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        if (session.getAttribute("loginTrue") == null)
        response.sendRedirect("login.jsp");
    %>
    <div style="text-align: center">
        <h1 style="color: darkcyan">Credentials</h1>
        <div id="BackToHomeDiv" style="text-align: left">
            <form action="home.jsp">
                <button id="backToHome" type="submit" class="btn btn-warning pull-right">Back to home page</button>
                <br>
            </form>
        </div>
        <form action="new" method="post">
            <input type="submit" value="+ Add Credentials" class="btb btn-info">
        </form>
    </div>
    <div class="table-responsive">
        <br>
        <table class="table table-striped" id="credentialsTable">
            <thead>
            <tr>
                <th style="width: 5%" scope="col"></th>
                <th style="width: 15%" scope="col"></th>
                <th style="width: 30%" scope="col">URL</th>
                <th style="width: 25%" scope="col">Username</th>
                <th style="width: 25%" scope="col">Password</th>
            </tr>
            </thead>
            <tbody>
            <sql:setDataSource driver="com.mysql.cj.jdbc.Driver" url="jdbc:mysql://localhost:3306/cloud_schema"
                               user="root"
                               password="slivinska1999"/>
            <sql:query var="rs">select * from credentials where userid = '${userid}'</sql:query>
            <c:forEach items="${rs.rows}" var="credentials">
                <tr>
                    <td>
                        <form action="edit">
                            <input type="hidden" name="credentialId"
                                   value="<c:out value='${credentials.credentialid}'/>"/>
                            <button class="btn btn-success" id="edit" type="submit">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="delete">
                            <input type="hidden" name="credentialId"
                                   value="<c:out value='${credentials.credentialid}'/>"/>
                            <button class="btn btn-danger" id="delete" type="submit">Delete</button>
                        </form>
                    </td>
                    <td><c:out value="${credentials.url}"></c:out></td>
                    <td><c:out value="${credentials.username}"></c:out></td>
                    <td><input style="width: 150px" type="password"
                               value="<c:out value="${credentials.password}"></c:out>"></td>
                </tr>
            </c:forEach>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
