<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 10.02.2021
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Title</title>
</head>
<body>
<div>
    <% if (session.getAttribute("loginTrue") == null)
        response.sendRedirect("login.jsp");
    %>
    <c:if test="${note != null}">
    <form action="updateNote" name="note" method="post">
        </c:if>
        <c:if test="${note == null}">
            <form action="newNote" name="note" method="post"
        </c:if>
        <caption>
            <h2 style="text-align:center">
                <c:if test="${note != null}">
                    Edit Note
                </c:if>
                <c:if test="${note == null}">
                    Add New Note
                </c:if>
            </h2>
        </caption>

        <c:if test="${note != null}">
            <input type="hidden" name="noteid" value="<c:out value='${note.noteId}'/>"/>
        </c:if>
            <br>
        <div  class="container col-md-5; center-block" >
            <fieldset  class="form-group">
                <label>Note Title: </label> <input type="text" value="<c:out value='${note.noteTitle}' />"
                                                   class="form-control"
                                                   name="noteTitle" required="required">
            </fieldset>

            <fieldset class="form-group">
                <label>Note Decription: </label> <input type="text" value="<c:out value='${note.noteDescription}' />"
                                                        class="form-control"
                                                        name="noteDescription">
            </fieldset>
            <br>
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
