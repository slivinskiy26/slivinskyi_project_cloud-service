<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ page import="com.dataart.slivinskyi_cloud_service.dao.NoteDAO" %>
<%@ page import="com.dataart.slivinskyi_cloud_service.model.User" %>
<%@ page import="com.dataart.slivinskyi_cloud_service.dao.UserDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="com.dataart.slivinskyi_cloud_service.model.Note" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.stream.Collectors" %><%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 09.02.2021
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Notes</title>
</head>
<body>
<div>
    <div style="text-align: center">
        <h1 style=" color:coral">Notes</h1>
        <c:if test="${successAddNote != null}">
            <div id="success-msg" class="alert alert-dark">
                    ${successAddNote}
            </div>
        </c:if>
        <c:if test="${successUpdateNote != null}">
            <div id="success-msg" class="alert alert-dark">
                    ${successUpdateNote}
            </div>
        </c:if>
        <c:if test="${successDeleteNote != null}">
            <div id="success-msg" class="alert alert-dark">
                    ${successDeleteNote}
            </div>
        </c:if>
    </div>
    <div style="text-align: center">
        <div id="BackToHomeDiv" style="text-align: left">
            <form action="home.jsp">
                <button id="backToHome" type="submit" class="btn btn-warning pull-right">Back to home page</button>
                <br>
            </form>
        </div>
        <form action="newNote" method="get">
            <button type="submit" id="newNote" class="btb btn-info">+ Add Note</button>
        </form>
        <br>
    </div>
    <% if (session.getAttribute("loginTrue") == null)
        response.sendRedirect("login.jsp");
    %>
    <div class="table-responsive">
        <table class="table table-striped" id="noteTable">
            <thead>
            <tr>
                <th style="width: 5%" scope="col"></th>
                <th style="width: 15%" scope="col"></th>
                <th style="width: 20%" scope="col">Title</th>
                <th style="width: 60%" scope="col">Description</th>
            </tr>
            </thead>
            <tbody>
            <sql:setDataSource var="db" driver="com.mysql.cj.jdbc.Driver" url="jdbc:mysql://localhost:3306/cloud_schema"
                               user="root" password="slivinska1999"/>
            <sql:query var="rs" dataSource="${db}">select * from notes where userid = '${userid}'</sql:query>
            <c:forEach items="${rs.rows}" var="note">
                <tr>
                    <td>
                        <form action="updateNote" method="get">
                            <input type="hidden" name="noteid" value="<c:out value='${note.noteid}'/>"/>
                            <button class="btn btn-success" id="editNote" type="submit">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="deleteNote" method="get">
                            <input type="hidden" name="noteid" value="<c:out value='${note.noteid}'/>"/>
                            <button class="btn btn-danger" id="deleteNote" type="submit">Delete</button>
                        </form>
                    </td>
                    <td scope="row" class="note-title"><c:out value="${note.notetitle}"></c:out></td>
                    <td><c:out value="${note.notedescription}"></c:out></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
