<%@ page import="com.dataart.slivinskyi_cloud_service.service.EncryptionService" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 15.02.2021
  Time: 13:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Title</title>
</head>
<body>
<div>
    <% if (session.getAttribute("loginTrue") == null)
        response.sendRedirect("login.jsp");
    %>
    <c:if test="${credentials != null}">
    <form action="updateCredentials" method="post">
        </c:if>
        <c:if test="${credentials == null}">
            <form action="addCredentials" method="post"
        </c:if>
        <caption>
            <h2 style="text-align: center">
                <c:if test="${credentials != null}">
                    Edit Credentials
                </c:if>
                <c:if test="${credentials == null}">
                    Add New Credentials
                </c:if>
            </h2>
        </caption>

        <c:if test="${credentials != null}">
            <input type="hidden" name="credentialId" value="<c:out value='${credentials.credentialId}'/>"/>
        </c:if>

        <div class="container col-md-5; center-block">
            <fieldset class="form-group">
                <label>Credentials URL: </label> <input type="text" value="<c:out value='${credentials.url}' />"
                                                        class="form-control"
                                                        name="credentialsURL" required="required">
            </fieldset>

            <fieldset class="form-group">
                <label>username: </label> <input type="text" value="<c:out value='${credentials.username}' />"
                                                 class="form-control"
                                                 name="credentialsUsername">
            </fieldset>

            <fieldset class="form-group">
                <label>password: </label> <input type="text" value="<c:out value='${decryptedPassword}' />"
                                                 class="form-control"
                                                 name="credentialsPassword">
            </fieldset>
            <br>
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
