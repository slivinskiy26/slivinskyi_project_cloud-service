<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 09.02.2021
  Time: 13:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Title</title>
</head>
<body>
<div>
<%    if (session.getAttribute("loginTrue") == null)
    response.sendRedirect("login.jsp");
%>
<c:if test="${errorAddNote != null}">
    <h4>${errorAddNote}</h4>
</c:if>
<c:if test="${errorUpdateNote != null}">
    <h4>${errorUpdateNote}</h4>
</c:if>
<c:if test="${errorDeleteNote != null}">
    <h4>${errorDeleteNote}</h4>
</c:if>
<c:if test="${errorUploadFile != null}">
    <h4>${errorUploadFile}</h4>
</c:if>
<c:if test="${errorDownloadFile != null}">
    <h4>${errorDownloadFile}</h4>
</c:if>
<c:if test="${errorDownloadFile != null}">
    <h4>${errorDownloadFile}</h4>
</c:if>
<c:if test="${errorAddCredentials != null}">
    <h4>${errorAddCredentials}</h4>
</c:if>
<c:if test="${errorUpdateCredentials != null}">
    <h4>${errorUpdateCredentials}</h4>
</c:if>
<c:if test="${errorDeleteCredentials != null}">
    <h4>${errorDeleteCredentials}</h4>
</c:if>
<a href="home.jsp" >Back to home page!</a>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
