<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 05.02.2021
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Title</title>
    <script type="text/javascript">
        function validate(){
            var username = document.getElementById("username");
            var password = document.getElementById("password");

            if (username.value.trim()==""){
                alert("Blank Last Username")
                username.style.border = "solid 3px red";
                return false;
            }else if (password.value.trim()==""){
                alert("Blank Password")
                password.style.border = "solid 3px red";
                return false;
            }else if (password.value.trim().length < 7){
                alert("Password too short. \nPassword should contain at least 7 characters.")
                password.style.border = "solid 3px red";
                return false;
            } else {
                return true;
            }
        }
    </script>
</head>
<body>
<div style="text-align: center">
    <h1>Login</h1>
    <c:if test="${errorMessage != null}">
        <div id="error-msg" class="alert alert-dark">
                ${errorMessage}
        </div>
    </c:if>
    <c:if test="${IncorrectUsername != null}">
        <div id="error-msg" class="alert alert-dark">
                ${IncorrectUsername}
        </div>
    </c:if>
    <c:if test="${successSignUp != null}">
        <div id="success-msg" class="alert alert-dark">
                ${successSignUp}
        </div>
    </c:if>
    <form onsubmit="return validate();" action="login" method="post">
        username: <input type="text" name="username" id="username"><br>
        <br>
        password: <input type="password" name="password" id="password"><br>
        <br>
        <button id="login" type="submit" class="btn btn-info ">Login</button>
    </form>
    <label> <a id="signup-link" href="signup.jsp">Click here to sign up</a> </label>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
