<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sslivinskyi
  Date: 09.02.2021
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" media="all"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>Files</title>
</head>
<body>
<div style="text-align: center">
    <c:if test="${successUploadFile != null}">
        <h4>${successUploadFile}</h4>
    </c:if>
    <c:if test="${successDeleteFile != null}">
        <h4>${successDeleteFile}</h4>
    </c:if>
    <h1 style="color: cornflowerblue">Files</h1>
    <div id="BackToHomeDiv" style="text-align: left">
        <form action="home.jsp">
            <button id="backToHome" type="submit" class="btn btn-warning pull-right">Back to home page</button><br>
        </form>
    </div>
    <div>
        <form method="POST" enctype="multipart/form-data" action="fileUpload">
            <input type="file" name="file" id="file" class="center-block"><br>
            File Description: <input type="text" name="description"><br>
            <br>
            <input type="submit" value="Upload" class="btn btn-success">
        </form>
    </div>

    <% if (session.getAttribute("loginTrue") == null)
        response.sendRedirect("login.jsp");
    %>
    <table class="table table-striped" id="noteTable">
        <thead>
        <tr>
            <th style="width:5%" scope="col"></th>
            <th style="width: 15%" scope="col"></th>
            <th style="width: 25%" scope="col">File name</th>
            <th style="width: 55%" scope="col">File description</th>
        </tr>
        </thead>
        <tbody>
        <sql:setDataSource driver="com.mysql.cj.jdbc.Driver" url="jdbc:mysql://localhost:3306/cloud_schema" user="root"
                           password="slivinska1999"/>
        <sql:query var="rs">select * from files where userid = '${userid}'</sql:query>
        <c:forEach items="${rs.rows}" var="file">
            <tr>
                <td>
                    <form action="fileDownload" method="post" target="_blank" rel="noopener noreferrer">
                        <input type="hidden" name="fileid" value="<c:out value='${file.fileid}'/>"/>
                        <button class="btn btn-success" id="download" type="submit">View</button>
                    </form>
                </td>
                <td>
                    <form action="fileDelete" method="post">
                        <input type="hidden" name="fileid" value="<c:out value='${file.fileid}'/>"/>
                        <button class="btn btn-danger" id="delete" type="submit">Delete</button>
                    </form>
                </td>
                <td><c:out value="${file.filename}"></c:out></td>
                <td><c:out value="${file.description}"></c:out></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
